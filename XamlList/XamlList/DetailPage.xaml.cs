﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamlList
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetailPage : ContentPage
	{
		public DetailPage()
		{
			InitializeComponent();
		}
	}
}
