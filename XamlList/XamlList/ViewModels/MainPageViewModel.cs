﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace XamlList.ViewModels
{
	public class MainPageViewModel : INotifyPropertyChanged
	{
		public MainPageViewModel()
		{
			AllNotes = new ObservableCollection<string>();
			EraseCommand = new Command(() =>
			{
				TheNote = String.Empty;
			});
			SaveCommand = new Command(() =>
			{
				if (!String.IsNullOrWhiteSpace(TheNote))
				{
					AllNotes.Add(TheNote);
					TheNote = String.Empty;
				}
			});
			SelectedNoteChangedCommand = new Command(async () =>
			{
				var detailPageViewModel = new DetailPageViewModel(SelectedNote);
				var detailPage = new DetailPage();
				detailPage.BindingContext = detailPageViewModel;

				await Application.Current.MainPage.Navigation.PushModalAsync(detailPage);
			});
			ClearAllCommand = new Command(() =>
			{
				AllNotes.Clear();
			});
			SaveToFileCommand = new Command(() =>
			{
				var folderName = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
				var fileName = Path.Combine(folderName, "temp.txt");
				File.WriteAllLines(fileName, AllNotes);
			});
			LoadFromFileCommand = new Command(() =>
			{
				var folderName = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
				var fileName = Path.Combine(folderName, "temp.txt");
				var lines = File.ReadAllLines(fileName).ToList();
				AllNotes.Clear();
				foreach (var line in lines)
                {
					AllNotes.Add(line);
                }
			});
		}

		public ObservableCollection<string> AllNotes { get; set; }

		public event PropertyChangedEventHandler PropertyChanged;

		string _selectedNote;
		public string SelectedNote
		{
			get => _selectedNote;
			set
			{
				_selectedNote = value;
				var args = new PropertyChangedEventArgs(nameof(SelectedNote));
				PropertyChanged?.Invoke(this, args);
			}
		}

		string _theNote;
		public string TheNote
		{
			get => _theNote;
			set
			{
				_theNote = value;
				var args = new PropertyChangedEventArgs(nameof(TheNote));
				PropertyChanged?.Invoke(this, args);
			}
		}

		public Command SaveCommand { get; }
		public Command EraseCommand { get; }
		public Command ClearAllCommand { get; }
		public Command SelectedNoteChangedCommand { get; }
		public Command SaveToFileCommand { get; }
		public Command LoadFromFileCommand { get; }
	}
}
