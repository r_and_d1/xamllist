﻿using System.ComponentModel;
using Xamarin.Forms;

namespace XamlList.ViewModels
{
	public class DetailPageViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		public DetailPageViewModel(string note)
		{
			DismissPageCommand = new Command(async () =>
			{
				await Application.Current.MainPage.Navigation.PopModalAsync();
			});
			NoteText = note;
		}

		string _noteText;
		public string NoteText
		{
			get => _noteText;
			set
			{
				_noteText = value;
				var args = new PropertyChangedEventArgs(nameof(NoteText));
				PropertyChanged?.Invoke(this, args);
			}
		}

		public Command DismissPageCommand { get; }
	}
}
